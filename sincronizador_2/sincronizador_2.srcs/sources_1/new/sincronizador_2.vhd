----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.12.2018 16:51:02
-- Design Name: 
-- Module Name: sincronizador_2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sincronizador_2 is
Port (
clk: in std_logic;
reset: in std_logic;
sync_in: in std_logic;
sync_out: out std_logic
 );
end sincronizador_2;

architecture Behavioral of sincronizador_2 is

begin
p1:process(clk,reset)
  begin
  if reset='1' then
   sync_out<='0';
  elsif rising_edge(clk) then 
   sync_out<=sync_in;
  end if;
 end process;



end Behavioral;
